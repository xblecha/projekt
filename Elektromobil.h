//
// Created by david.blecha on 26.04.2020.
//

#ifndef TEST1ZNOVU_ELEKTROMOBIL_H
#define TEST1ZNOVU_ELEKTROMOBIL_H

#include <iostream>

class Elektromobil {
private:
    std::string m_znacka;
    int m_kapacitaBaterie;
    int m_nabito;
    int m_nabijeciProud;

public:
    Elektromobil(std::string znacka, int kapacitaBaterie, int nabito, int nabijeciProud);
    int getKapacitaBaterie();
    int getNabito();
    int getNabijeciProud();
    void setNabito(int kolik);
};





#endif //TEST1ZNOVU_ELEKTROMOBIL_H
