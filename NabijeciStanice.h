//
// Created by david.blecha on 25.04.2020.
//

#ifndef TEST1ZNOVU_NABIJECISTANICE_H
#define TEST1ZNOVU_NABIJECISTANICE_H
#include <iostream>
#include "Elektromobil.h"

class NabijeciStanice {
private:
    int m_maxKapacita;
    int m_stavNabiti;
    int m_maxNabijeciProud;

public:
    std::string m_logger;
    NabijeciStanice(int kapacita, int nabijeciProud);
    int getStavNabiti();
    void zacniDobijet(Elektromobil * elektromobil);
    int poterbaDobit(Elektromobil * elektromobil);

};


#endif //TEST1ZNOVU_NABIJECISTANICE_H
