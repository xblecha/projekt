//
// Created by david.blecha on 25.04.2020.
//

#include "NabijeciStanice.h"
#include "Elektromobil.h"
#include "Logger.h"

NabijeciStanice::NabijeciStanice(int kapacita, int nabijeciProud) {
    m_maxKapacita = kapacita;
    m_stavNabiti = 100;
    m_maxNabijeciProud = nabijeciProud;
}

int NabijeciStanice::getStavNabiti() {
    return m_stavNabiti;
}

void NabijeciStanice::zacniDobijet(Elektromobil*elektromobil) {
if (elektromobil->getNabijeciProud() < m_maxNabijeciProud) {
    Logger::pridejLog("Elekromobil nelze dobit");
} else {
    std::cout << "Bylo zahajeno dobijeni" << std::endl;
    elektromobil->setNabito(100);
}
}

int NabijeciStanice::poterbaDobit(Elektromobil*elektromobil) {
return elektromobil->getNabito();
}
