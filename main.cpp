#include <iostream>
#include "Logger.h"
#include "Elektromobil.h"
#include "NabijeciStanice.h"

int main() {

    NabijeciStanice * superCharger = new NabijeciStanice(1000, 100);

    Elektromobil * tesla = new Elektromobil("Tesla", 100, 50, 10);

    superCharger->zacniDobijet(tesla);
    Logger::vypisLog();

    delete superCharger;
    delete tesla;
    return 0;
}
