//
// Created by david.blecha on 26.04.2020.
//

#include "Elektromobil.h"

Elektromobil::Elektromobil(std::string znacka, int kapacitaBaterie, int nabito, int nabijeciProud) {
    m_znacka = znacka;
    m_kapacitaBaterie = kapacitaBaterie;
    m_nabito = nabito;
    m_nabijeciProud = nabijeciProud;
}

int Elektromobil::getKapacitaBaterie(){
    return m_kapacitaBaterie;
}

int Elektromobil::getNabito() {
    return m_nabito;
}

int Elektromobil::getNabijeciProud() {
    return m_nabijeciProud;
}

void Elektromobil::setNabito(int kolik) {
    m_nabito = kolik;
}