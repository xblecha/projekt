//
// Created by david.blecha on 26.04.2020.
//

#ifndef TEST1ZNOVU_LOGGER_H
#define TEST1ZNOVU_LOGGER_H
#include <iostream>

class Logger {
private:
    static std::string s_log;

public:
    static void pridejLog(std::string chybovaHlaska);
    static void vypisLog();

};




#endif //TEST1ZNOVU_LOGGER_H
